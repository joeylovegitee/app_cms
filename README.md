# APP_CMS

### 项目介绍

**小组协作式Axure项目 中山大学南方学院 文学与传媒学院 网络与新媒体专业 成员：余宗怡、周雅卿、汤玲萍、汤雨晴、周雨、杨少少、周子濠**


### 第一部分：产品概况

#### 产品理念

基于对汉服用户需求的洞察，以及弘扬传统文化的价值内涵，搭建一个沟通和交流的社区。以平台方式解决汉服小众问题，为同袍提供大量优质商品，是汇聚了服饰穿搭、妆容教程、传统文化知识、线下活动等汉服相关主题的内容社区。

#### 产品核心目标

近年来，各类古装偶像剧热播，《白蛇：缘起》等国漫崛起，《中国诗词大会》备受瞩目，总决赛收视率甚至与《新闻联播》相当，传统文化重新受到关注。汉服作为中华传统文化的重要符号，加速融入人们日常生活。相比5年前，B站国风兴趣圈层（国创、国风舞蹈、汉服等）覆盖人数增长20倍以上。

汉服社交APP集汉服设计搭配、购物种草与交流与一体，打造汉服文化交流平台。鉴别山正、购买汉服、交流社区、闲置二手。将汉服与其他中华传统文化融合，帮助人们深入了解传统文化，让传统文化融入生活。



#### 产品亮点

- 【鉴别山正】想知道你穿的汉服是山寨还是正品？汉服小白怕出门被笑？扫一扫立刻帮助你识别！

- 【同袍活动】定位同城线下活动！提供线下交友场所

- 【线上商城】提供各类品牌馆、个人店和汉服租赁、化妆及造型服务、摄影服务的汉服体验馆！

- 【汉服科普】分享汉服相关的入坑知识、妆容教程

#### 目标市场

1. 追求古典美的审美需求，受影视古装剧或传统文化综艺的影响，其消费的出发点是对美、新、异的追求。
2. 高层次文化内涵的需求，主要是对汉服所代表的汉文化内涵关注的人群，以具有一定文化修养的知识分子居多，他们认为汉服与礼法相辅相成，需要恢复和宣传的对象，并乐于身体力行。
3. 针对汉服有进行专门的研究讨论、喜欢摄影或文字分享交流学习各种汉服知识的汉服爱好者，以及为汉服爱好者提供便捷集中的细分市场的各类汉服商家入驻，形成良性的汉服及汉族文化圈。
4. 二线城市用户是主要的消费群体，三线城市用户群体消费贡献金额逐步提升。

#### 产品功能架构图

![产品功能架构图](https://gitee.com/joeylovegitee/app_cms/raw/master/image/%E6%B1%89%E6%9C%8DAPP%E4%BA%A7%E5%93%81%E5%8A%9F%E8%83%BD%E6%9E%B6%E6%9E%84%E5%9B%BE.png)

#### 产品页面图

![产品页面架构图](https://gitee.com/joeylovegitee/app_cms/raw/master/image/%E6%B1%89%E6%9C%8DAPP%E4%BA%A7%E5%93%81%E9%A1%B5%E9%9D%A2%E6%9E%B6%E6%9E%84%E5%9B%BE.png)

### 第二部分：市场调研

#### 市场调研数据
   在2020年淘宝造物节大众之选top10中前三名都是与汉服相关的服饰。
   据第一财经商业数据中心发布的《2020汉服消费趋势洞察报告》显示，2019年淘宝的汉服成交金额突破20亿；过去3年，天猫汉服行业增长6倍。
   汉服爱好者人数仅约200万人是一个相对小众的群体，但这一群体支撑的消费规模突破10亿元，说明这一群体的消费力强并且消费热情高、粘性大。
   汉服爱好者，平均年龄在18-24岁。汉服的购买群体普遍以90后、00后为主，他们或为职场的新生力量，或为尚在校园、家境小康的强购买力人群，有钱有闲加上对汉服审美的追求，成了购买汉服最大的原动。
   江苏、四川、北京、广东等地区的网民最为关注“汉服”相关资讯;其中，“汉服”在江苏的讨论热度最高，其次是四川及北京市。从地域角度，国内规模较大的汉服文化节有西塘文化节、国家(北京)汉服文化节、成都汉服文化节，说明汉服的网络热度与文化节的举办密切相关，且有较明显的地域集中性。

   2018年，女性“同袍”的占比达到88.2%。由此可见，目前中国汉服市场仍以女性消费者主导。

   2019年，约55.5%的汉服爱好者拥有2-4套的汉服，拥有5套汉服及以上的消费者数量约占15.3%，总体的汉服人均拥有量约为3件。


![Z世代](https://images.gitee.com/uploads/images/2020/1228/132507_06b7e5af_4876229.png "屏幕截图.png")

Z世代，喜爱汉族文化，有强烈身份认同感和归属感的需求，渴望安利和种草汉服的女性。


#### 目标市场定位
**中国汉服用户以女性为主，用户购买意愿强**
+ 2014年至2018年，中国汉服消费群体中女性的比例逐年升高，至2018年，女性“同袍”的占比达到88.2%。
+ 2019年，有5.72%的人表示在日常生活中穿过汉服，分别有13.13%和17.51%的人表示在传统节日或者是参加汉服专题活动时穿过汉服。（[数据来源：艾媒报告|2019-2021中国汉服产业数据调查、用户画像及前景分析报告](https://report.iimedia.cn/repo18-0/38943.html)）


#### 潜在市场
随着中国经济的发展以及人们对民族文化的追溯，在中国人口占绝对多数的汉族人口，是一个不可忽视的群体，同时也是一个不可估量的潜在市场。
+ 依据：
由于受当前全球金融危机的影响，中国纺织服装业的出口外贸业受到了前所未有的冲击。国内部分服装企业不得不面临由依靠外贸出口转向国内销售。与此同时，汉服市场还是个有待发展成熟的市场，远远没有达到饱和的状态。况且，“汉服热”的潮流还在不断地持续高涨，给商家提供了十分有利的发展空间。

#### 消费主体定位
一是具有强烈民族文化意识的具有购买能力的中、轻年汉族人。
二是少年儿童(重要突破口)。
+ 依据：
1. 目前进行得如火如荼的汉服运动，参加人群主要是收入比较稳定具有购买能力的中年人以及具有一定消费能力的年轻人。
2. 汉服外形飘逸美观,色彩鲜艳，符合儿童的审美观。学龄前儿童尚未进入学校学习，穿着汉服不会对生活造成诸多不便。儿童服装市场进入要求较低,容易突破。

#### 市场需求
+ （1）学生群体。随着“汉服热”、“古风审美”等传统文化复兴潮流兴起，加上古装影视剧的热播，汉服产业这一小众市场正逐渐走向大众视野。与此同时，在消费升级的大背景下，消费者的需求也越来越趋向于个性化，具有特色的汉服开始大放光彩，成为消费者的宠儿，走在街上经常可以看到着汉服出行的年轻人。曾经只被少数人当做爱好的汉服正迅速从小众圈层走向大众化，并迅速成长为一个较大的产业。
+ （2）儿童童装市场。中国家长舍得为孩子花钱，漂漂亮亮的汉服童装很有市场。
+ （3）婚庆市场。对比普遍流行的西式婚纱、西装婚礼，随着“汉服热等传统文化复兴浪潮愈演愈烈，越来越多年轻人选择汉服作为婚礼服。
+ （4）个性化娱乐或个性化需求，带有传统文化色彩的行业，例如国风音乐、舞蹈演出需要等。

#### 商业价值
（1）**中国汉服需求市场在不断扩大，消费者用户粘性大**
+ 近年来，中国的汉服爱好者人数快速扩增，在2018年汉服爱好者数量达到204.2万人，同比增长72.9%。
+人数仅约200万人的汉服爱好者群体，支撑着突破10亿元的消费规模，一定程度上反映了这一群体的消费力强并且消费热情高、粘性大。（[数据来源：艾媒报告|2019-2021中国汉服产业数据调查、用户画像及前景分析报告](https://report.iimedia.cn/repo18-0/38943.html)）

（2）**个性化、二次元、古今融合的国潮消费带动汉服产业的发展**
+ 随着消费升级大背景的影响，消费者们的需求也越趋个性化，而能满足不同需求的产品也能表现出更高的溢价能力。
+ 个性化需求对企业的产业结构也提出了相应的要求。在未来，应对市场的差异化需求，品牌或商家（平台）需要拓展更多的专题活动场景来吸引消费者。满足消费者复杂的个性化需求，同时是在推动汉服市场规模的增长。

#### 盈利化
+ iiMedia Research（艾媒咨询）数据显示，在2018年汉服爱好者数量达到204.2万人，同比增长72.9%。而市场销售规模已经突破10亿，达到10.8亿元，未来2-3年汉服市场仍将保持增长态势， 2020年销售额预计将达16.9亿元。（[数据来源：艾媒网《2020年中国汉服专题研究报告》案例征集](https://www.iimedia.cn/c460/67867.html)）

![2014-2018中汉服市场消费人群数据调查](http://img.iimedia.cn/000014dcf37f535c23af2009a8a801322cc3cd993922942d01032a2f486dff88357ff)

![2015-2020中国汉服市场营销额数据预测](http://img.iimedia.cn/1000109af6205a12927d6d2bbd0e213d4607bb54e5d490491307e9af7acffbf1653fa)

### 第三部分：竞品分析

根据不完全统计，现在面向我们汉服同袍的社群APP已有十多个，按应用市场下载量数据来看已经成气候的主要是汉服荟、同袍。
##### 1. 竞品选择

| 汉服荟                                                       | 同袍                                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
|![汉服荟](https://images.gitee.com/uploads/images/2020/1229/103213_0abdd43e_4876229.png "屏幕截图.png") | ![同袍](https://images.gitee.com/uploads/images/2020/1229/103151_ac7d3d0a_4876229.png "屏幕截图.png")|
| 选择理由：汉服荟成立于2014年，算是开创了汉服社群APP的先河，经过这几年的积累，运营已经非常稳定成熟，也是目前当之无愧的汉服APP一哥。 | 选择理由：同袍成立于2018年底，板块分细致齐全，可学习汉服知识，并具有汉服社交圈的属性。 |

##### 2.战略层分析

|        | 产品定位                                                     | 产品优势                                                     | 产品劣势                                                     |
| ------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 汉服荟 | 一款结合社交元素与新媒体元素，具有摄影、交流、购物功能的汉服同袍社区软件 | 国内最早一批开创汉服社群先河app代表；新上线的汉服购商城可以直接下单交易，将筛选更多的优质汉服商家入驻，提供更专业的购物体验。 | 后期，更多用户涌入，但是用户质量参差不齐，汉服原本高质量的社群氛围受到稀释。此外平台未采取有效措施策展补救。 |
| 同袍   | 一款专为汉服同袍打造的汉服APP。具有社交、活动、知识文章、好物兑换四大功能分类|同袍APP是最早入局汉服文化社区市场的平台之一，具有先发优势，早期积聚了大量汉服爱好者。主打功能有汉服活动组织，便于组织较大的活动组织和报名；通过签到、发布动态等互动可得铜钱可兑换商品|活跃用户数量较少，盈利模式尚不明朗|

##### 3. 范围层分析

|| 汉服荟 | 同袍 |
| --- | --- | --- |
| 主要功能 | 闲置二手：提供一个平台让二手汉服不再闲置<br>购买汉服：聚集最全正规汉服品牌，提供更专业的购物体验。|社交分享：分享一切汉服日常。找社团，交朋友，发悬赏，和广大同袍一起在线交流互动<br>汉服活动：活动版块提供活动发布，活动报名等功能，包含各种汉服线上线下活动<br>好物兑换：通过签到、发布动态等互动即可得铜钱，在兑换区免费兑换汉服周边商品
| 基本功能 | 寻找同袍：寻找与自己志同道合的汉服同袍<br>讨论话题：讨论关于汉服的争议、困惑，关于汉服搭配、汉服活动等。<br>汉服学堂：科普关于汉服古今中外的知识 | 话题讨论：用户可以根据话题参与讨论<br>动态分享：包括视频、心情、摄影、音乐等内容的分享 |
| 是否付费 | 全平台功能免费使用；| 部分付费，可充值为铜钱，可用来兑换汉服配饰、发布悬赏 |


**4.结构层分析（产品框架图）**

![竞品架构图](https://images.gitee.com/uploads/images/2020/1229/170009_d8db869a_2230291.png "竞品架构图.png")

**5.表现层分析**

**汉服荟**

- 首页

![首页](https://images.gitee.com/uploads/images/2020/1229/205840_c1b8cd5a_4876229.png "屏幕截图.png")

- 商城

![商城](https://images.gitee.com/uploads/images/2020/1229/210329_2b376fed_4876229.png "屏幕截图.png")


- 发现

![发现](https://images.gitee.com/uploads/images/2020/1229/205914_c12a7234_4876229.png "屏幕截图.png")

**同袍**
- 整体界面设计
![整体界面设计](https://images.gitee.com/uploads/images/2020/1229/235339_4933ddde_2230291.png "整体界面设计.png")
- 首页
![首页](https://images.gitee.com/uploads/images/2020/1229/235652_03369e7a_2230291.png "屏幕截图.png")
- 商城
![商城](https://images.gitee.com/uploads/images/2020/1229/235614_999931a5_2230291.png "屏幕截图.png")
- 发现
![发现](https://images.gitee.com/uploads/images/2020/1229/235742_4cea92a2_2230291.png "屏幕截图.png")

### 第四部分：用户分析
#### 4.1目标用户
1. 主要为95后、00后的女性用户，有一定的购买力和有闲情雅致，对汉服有专门研究讨论、追求审美艺术的汉服爱好者
2. 追求古典美的审美需求，国潮文化的兴起，受古典古装影视剧或传统文化综艺的深层影响的人
3. 有一定的文化素养的高端人士，对中国传统文化的深度认同，对古代生活习俗向往，希望身体力行宣扬汉服之美

#### 4.2用户痛点

1. 对汉服形制知识不了解，不知道不同款式服装的穿法
2. 对汉服发型、妆容无从下手
3. 对汉服品牌了解不深，害怕买到山寨汉服
4. 身边喜欢汉服的朋友不多，无法相互交流
5. 想参加汉服相关活动却找不到信息渠道
6. 想要以轻松的方式来学习汉服中承载的中国传统文化
7. 汉服店铺良莠不齐，不确定成衣质量

#### 4.3用户需求分析、用户场景 

|序号 |  需求分析  | 用户场景  |
|  ----  |  ----  |  ----  |
| 1       |    看到喜欢的汉服想要搜同款、查看相关品牌    | 用扫一扫功能获取服装资讯    |
| 2       |    新入汉服坑的小白，对汉服的了解不深，希望通过有鉴别汉服的山正软件来避坑。    | 用扫一扫功能来鉴别山正    |
| 3        |  现实生活中热爱汉服的人较少，望寻找社区同袍一起交流     |   使用主页的交流社区与汉服爱好者交流，查看穿搭指南、安利、测评等  |
| 4     |    参与线上、线下汉服活动，享受同袍齐聚的欢乐。   |   查看同城举办过或报名正进行的汉服线下活动  |
| 5     |   想要化妆师帮忙做汉服妆造   |   查看服务预约，选择需要的服务  |
| 6      |  不喜欢、不合适的服饰想出售；想入已绝版的汉服     | 使用二手闲置交易    |
| 7    |   希望不只是通过海量枯燥文章来了解汉服知识    |  使用“发现”中的知识科普模块，通过诗词曲赋鉴赏、主题知识竞赛来了解   |

#### 4.4用户画像
- 根据目标用户群体，我们选择了两类主要消费人群进行用户画像研究。
![汉服APP用户画像](https://images.gitee.com/uploads/images/2020/1230/003511_058d2ec8_2229427.png "汉服用户画像.png")
#### 用户流程图
![汉服APP用户流程图](https://gitee.com/joeylovegitee/app_cms/raw/master/image/%E7%94%A8%E6%88%B7%E6%B5%81%E7%A8%8B%E5%9B%BE.png)

### CMS内容
##### [CMS后台用户流程图](http://joeylovegitee.gitee.io/app_team/#id=2pnueo&p=%E7%AE%A1%E7%90%86%E5%90%8E%E5%8F%B0%E4%BD%BF%E7%94%A8%E6%B5%81%E7%A8%8B%E5%9B%BE&g=1)
##### 前端产品与CMS后台的交互逻辑（交互性说明）：
1.内容管理

（1）文章列表
+ 对应管理前端用户分享出来的内容（图文视频等）

![内容交互逻辑](https://images.gitee.com/uploads/images/2021/0122/234251_f01f166c_2231078.png "内容交互逻辑")

（2）活动分类
+ 对应管理前端产品“发现页”中线下服务活动的相关内容

2.商城管理

（1）商城管理
+ 对前端产品“商城”内容进行编辑管理
+ 管理前端产品“品牌馆”“商品详情页”等相关内容

![商城交互逻辑](https://images.gitee.com/uploads/images/2021/0122/224853_5d895ca4_2231078.png "商城交互逻辑")

3.用户管理

（1）用户列表、用户详情
+ 管理前端用户角色权限、资料等

4.系统管理

（1）App界面管理
+ 管理前端界面主题样式、颜色等相关内容

##### [CMS后台原型](http://joeylovegitee.gitee.io/app_cms)

### 参与贡献
#### 成员周雨
1. 与周子濠合作负责产品PRD文档的市场调研部分
2. 负责修改产品功能架构图、产品页面图
3. 与余宗怡合作制作cms后台各页面原型
4. 与余宗怡合作制作cms产品架构图和cms角色权限架构图
5. 负责cms后台原型的交互部分
6. 与余宗怡合作撰写前端产品与cms后台的交互逻辑

#### 成员周雅卿
1. 整理他人意见后，与周雨共同更新完善后的产品架构图
2. 与汤玲萍共同完成前端页面布局设计，本人负责的部分为“商城”和“我的”
3. 设计产品logo
4. 与汤玲萍共同完成产品文档中PRD1理论加值和PRD5前端产品逻辑性两部分

#### 成员余宗怡
1. 与周雨合作制作cms产品架构图、角色权限图
2. 与周雨合作制作cms后台各页面原型
3. 与周雨合作撰写前端产品与cms后台的交互逻辑
4. 准备路演和演讲并合作制作MRD

#### 成员杨少少

1. 与汤雨晴合作负责前端页面交互功能设计，本人负责【首页】、【发现】、【登录】、【注册】部分交互逻辑
2. 与汤雨晴合作负责产品PRD文档的竞品分析部分，本人负责”汉服荟“竞品的分析
3. 与汤玲萍、周雅卿、汤雨晴负责前端仓库交互文字说明，本人负责【我的】交互文字说明和汇总
4. 与汤雨晴合作负责前端仓库 Wiki界面&交互设计 文档书写
5. 完成发布者使用流程图

#### 成员汤雨晴
1. 整理修改产品功能架构图，产品页面架构图
2. 与杨少少合作负责前端页面交互功能设计，本人负责【商城】【我的】【发布】【设置】部分交互
3. 与杨少少合作负责产品PRD文档的竞品分析部分，本人负责“同袍”竞品的分析
4. 与汤玲萍、周雅卿、杨少少负责前端仓库交互文字说明，本人负责【发现】交互文字说明和汇总
5. 与杨少少合作负责前端仓库 Wiki界面&交互设计文档书写

#### 成员汤玲萍
1. 与周雅卿合作负责产品PRD文档的用户分析部分
2. 与周雅卿合作完成前端产品原型图，本人负责【注册】、【主页】、【发现】相关页面
3. 与周雅卿合作完成产品文档PRD1，本人负责游戏化设计说明
4. 与周雅卿、汤雨晴、杨少少完成PRD5前端产品逻辑，本人负责【主页】逻辑文字说明

#### 成员周子濠
1. 与周雨合作负责产品PRD文档的市场调研部分
2. 负责浏览者-流程图和CMS后台用户流程图
3. 负责用户流程图